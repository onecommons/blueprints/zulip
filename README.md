## Application Blueprint for Zulip

### What is Zulip?

[Zulip](https://zulip.com/) is an open source chat and collaborative
software, it a free and open source alternative to Slack.  This
blueprint is based on
[docker-zulip](https://hub.docker.com/r/zulip/docker-zulip) container
together with
[zulip-postgresql](https://hub.docker.com/r/zulip/zulip-postgresql), a
postgress database with Zulip extensions.

### Filling out a deployment blueprint
For the general steps to deploy an application with Unfurl Cloud, see
our [Getting Started guides](https://unfurl.cloud/help#guides).

#### Details

- Database password

  Password for the Zulip container to authenticate to the Postgres
  container

- Memcached password

  Password for the Zulip container to authenticate to the Memcached
  container

- RabbitMQ password

  Password for the Zulip container to authenticate to the RabbitMQ
  container

- Zulip admin email

  Email address to use for the zulip user

- Database password

  Zulip database password.

- Subdomain

#### Components
Zulip requires the following compute resources:
- At least 2 CPU(s)
- At least 4GB of RAM
- At least 10GB of hard disk space

**DNS**
Supported DNS providers include:

- Google
- DigitalOcean
- AWS
- Azure
